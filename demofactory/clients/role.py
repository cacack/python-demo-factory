from demofactory.client import Client

class Role(Client):
    client_type = "role"

    def __init__(self,session,name):
        self._session = session
        self.name = name
