from demofactory.resource import Resource

class Role(Resource):
    client_type = "role"

    def __init__(self,session,name):
        self._session = session
        self.name = name
