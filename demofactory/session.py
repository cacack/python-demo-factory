import datetime
import uuid

import demofactory.clients
import demofactory.resources

class Session(object):

    def __init__(self):
        # Create an instance variable which holds unique values for this "session".
        self._session = {
            "key": uuid.uuid4(),
            "timestamp": datetime.datetime.now(datetime.timezone.utc),
        }

    def client(self,name):
        if name == "role": return demofactory.clients.role.Role(session=self._session,name=name)

    def resource(self,name):
        if name == "role": return demofactory.resources.role.Role(session=self._session,name=name)
